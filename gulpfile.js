var gulp = require('gulp');
var sass = require('gulp-sass');

var uglify = require('gulp-uglify');
var concat = require('gulp-concat');
var babel = require("gulp-babel");
var sourcemaps = require("gulp-sourcemaps");
var gnf = require('gulp-npm-files');

gulp.task('test', function () {print("hello")
});

gulp.task('copyNpmDependenciesOnly', function() {
    gulp.src(gnf(), {base:'./'}).pipe(gulp.dest('./build'));
});

gulp.task('copyAllNpmDependencies', function() {
    gulp.src(gnf(true), {base:'./'}).pipe(gulp.dest('./build'));
});

gulp.task('copyNpmDependenciesAtDifferentFolder', function() {
    gulp
        .src(gnf(null, './path/to/package.json'), {base:'./'})
        .pipe(gulp.dest('./build'));
});
gulp.task('copyAllNpmDependenciesAtDifferentFolder', function() {
    gulp
        .src(gnf(true, './path/to/package.json'), {base:'./'})
        .pipe(gulp.dest('./build'));
});

gulp.task('sass', function () {return gulp.src(['src/app/scss/**/*.scss','./src/app/sass/**/*.sass'])
    .pipe(sass()) //use gulp
    .pipe(concat('libr.css'))
    .pipe(gulp.dest('./src/assets/style'))
});

gulp.task('js', function (){
    return gulp.src([
        './src/app/js/services/**/*.js',
        './src/app/js/components/**/*.js',
        './src/app/js/app.js'
    ])
        .pipe(uglify())
        .pipe(concat('app.js'))
        .pipe(gulp.dest('./src/assets/js'))
});

gulp.task("babel", function () {
    return gulp.src("src/**/*.js")
        .pipe(sourcemaps.init())
        .pipe(babel())
        .pipe(concat("all.js"))
        .pipe(sourcemaps.write("."))
        .pipe(gulp.dest('./src/assets/js'));
});

gulp.watch('./src/app/**', function (event) {
    gulp.run('js');
    gulp.run('sass');
});