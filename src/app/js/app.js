function SomeClass() {
    this.iter = 0;

    setInterval(() => {
        this.iter++;
        console.log('current iteration: ' + this.iter);
    }, 1000);
}

var sc = new SomeClass();